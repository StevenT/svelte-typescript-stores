import { writable } from'svelte/store';

export class ApiEmpty {
    value= null;
    error= null;
    loading= false;
};
export class ApiLoading {
    value= null;
    error= null;
    loading= true;
}
export class ApiResult<T> {
    value: T;
    error= null;
    loading= false;
    constructor(value: T) {
        this.value = value;
    }
};
export class ApiError {
    value= null;
    error: string;
    loading= false;
    constructor(error: string) {
        this.error = error;
    }
};

export type Api<T> = ApiEmpty | ApiLoading | ApiResult<T> | ApiError;

export interface IStore1 {
    count: number,
}
export interface IStore2 {
    api: Api<string>,
}

const initialState1: IStore1 = {
    count: 0,
};
const initialState2: IStore2 = {
    api: new ApiEmpty(),
};

const st1 = writable(initialState1);
const st2 = writable(initialState2);

const store1 = {
    subscribe: st1.subscribe,
    increment: () => st1.update(n => ({...n, ...{count: n.count + 1}})),
    decrement: () => st1.update(n => ({...n, ...{count: n.count - 1}})),
};

const store2 = {
    subscribe: st2.subscribe,
    fetch: updateApi,
};


async function updateApi(id: number, n: IStore2) {
    const api = await fetchApi(id);
    return st2.update(n => ({...n, ...{api}}));
}

async function fetchApi(id: number) {
    try {
        const res = await fetch(`https://swapi.dev/api/people/${id}/`);
        const json = await res.json();
        if (res.ok) {
            console.debug('API response', json);
            return new ApiResult<string>(json)
        }
        console.error('error', json);
        throw new Error(json.detail);
    } catch(e) {
        console.error('Error on API: ', e);
        return new ApiError(e);
    }
}

const stores = {
    main: store1,
    api: store2,
};
export default stores;
